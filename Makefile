LIBS=-llapack -lblas -larmadillo -lcurses
OBJECTS= main.o lsm.o TUI.o

.cpp.o: 
	g++ -c $< $(LIBS) 

default: $(OBJECTS)
	g++ $(OBJECTS) $(LIBS) -o rlsm

git:
	git status; git add --all; git status; git commit -m "$m"; git push origin master;
