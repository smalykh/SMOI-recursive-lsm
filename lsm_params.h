
#ifndef LSM_PARAM_DEF
#define LSM_PARAM_DEF

struct lsm_params
{
	int  	n_iters;
	int  	n_inputs;
	double 	Du;	
	double 	Dr;
	double 	lambda;
};

int lsm_on_rand_data(double *b_init_vec, double *b_true_vec, lsm_params *params);

#endif
