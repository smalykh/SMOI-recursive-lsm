#include "TUI_priv.h"
#include "TUI.h"

 // This variables more comfortable make available for all function in this file

// Is true after receiving exit command
static bool Terminated;

// State machine var
static int Screen = 0;

// Last key pressed by user
static int LastKey = 0;

int Process(lsm_params *lsm_params) 
{
	static double b_init[5] = { 0 };
	static double b_true[5] = { 0 };

	switch (Screen) 
  	{
		case VAR_SCREEN: 
		{
			double a[2] = {lsm_params->Du, lsm_params->Dr}; 
			char* str[2] ={"Du", "Dr"}; 
			InputDataScreen(2, str, a, "Type Du and Dr");
			lsm_params->Du = a[0];
			lsm_params->Dr = a[1];
			break;
		}
		case ITER_SCREEN: 
		{
			double tmp = lsm_params->n_iters;;
			char* str[1] ={"Number of iteration"}; 
			InputDataScreen(1, str, &tmp, "Type number of iteration");
			lsm_params->n_iters = (int)tmp;
			break;
		}
		case B_TRUE_SCREEN: 
		{
			char* str[5] ={"b0", "b1", "b2", "b3", "b4"}; 
			InputDataScreen(5, str, b_true, "Type true b coeffs");
			break;
		}
		case B_INIT_SCREEN: 
		{
			char* str[5] ={"b0", "b1", "b2", "b3", "b4"}; 
			InputDataScreen(5, str, b_init, "Type init b coefs");
			break;
		}
		case LAMBDA_SCREEN: 
		{
			char* str[1] ={"Lambda"}; 
			InputDataScreen(1, str, &(lsm_params->lambda), "Type lambda");
			break;
		}
		case LSM_SCREEN: 
		{
			lsm_params->n_inputs = 4;
			if(!TUI_RLSM(b_init, b_true, lsm_params))
			{
				NotificationScreen("\t\t\tb0\t\t     b1\t\t        b2\t\t     b3\t\t       b4\t\t   sqr error", "b.dat");
			}
			else
			{
				NotificationScreen("Error in input arguments. q - Back to menu", NULL);
			}
			break;
		}
		case PLOTTER_SCREEN:
		{
			TUI_Plotter();
			NotificationScreen("Plotter Works..", NULL);
			break;
		}
	
		default: ProcessScreenMain();
  	}
  
  	if (LastKey == KEY_q) Terminated = true;
}

int ProcessScreenMain() 
{
	static char *MenuItems[1024];
	static char *HeaderText;
	static char *StatusText;

 	MenuItems[0] = "Set Du and Dr";
  	MenuItems[1] = "Set amount of iterations";
 	MenuItems[2] = "Set true b coeffs";
 	MenuItems[3] = "Set init b coeffs";
 	MenuItems[4] = "Set lambda";
 	MenuItems[5] = "Run recursive LSM";
 	MenuItems[6] = "Run plotter";
 	
	HeaderText = "  q:Quit  ";
  	StatusText = "Tumba Umba";
  

	static int SelItem = 0;
	static int LastItem = 6; 
  	int MainMenuBoard = 15;
	
	// Clear screen
  	attrset(COLOR_PAIR(1));
  	clear();

  	// Draw header
  	attrset(A_BOLD|COLOR_PAIR(2));
  	ClearLine(0, MaxX);
  	mvaddstr(0, 0, HeaderText);
  
 	// Draw body
  	attrset(COLOR_PAIR(1));

  	for (int i = 0; i <= LastItem; i++) 
	{
    		if (SelItem == i) attrset(COLOR_PAIR(3));
		else  		  attrset(COLOR_PAIR(1));
    		ClearLine(1 + i, MaxX);
   		mvaddstr (1 + i, MainMenuBoard, MenuItems[i]);
  	}

  	// Draw status line
  	attrset(A_BOLD|COLOR_PAIR(2));
  	ClearLine(MaxY - 2, MaxX);
  	mvaddstr(MaxY - 2, 0, StatusText);

  	curs_set(0);
  	refresh();
  
	// Key handler
  	LastKey = getch();
  
 	if (LastKey == KEY_UP) 		SelItem--;
  	if (LastKey == KEY_DOWN) 	SelItem++;
 	if (LastKey == KEY_ENTER)	Screen = SelItem + 1;

	// Check overflow and underflow
  	if (SelItem > LastItem) SelItem = LastItem;
  	if (SelItem < 0) 	SelItem = 0;
}

int InputDataScreen(int n, char **labels, double *outputs, char * header)
{
 	clear(); 
 
	// Calculate sizes of objects
	int left_border  = 20;
	int rigth_border = 20;
	int up_border	 = 2;
	int down_border  = 5;

	int x_size = getmaxx(stdscr);
	int window_size  = (x_size - left_border - rigth_border)/n;
	int text_size 	 = 0.6 * window_size;

	// Draw header
  	ClearLine(0, x_size);
  	mvaddstr(0, 0, header);	

	// Draw labels and textboxes	
	char empty [x_size];
	memset(empty, ' ', sizeof(char)*text_size);
	empty[text_size] = '\0';
	for(int i = 0; i < n; i++)
	{
 		mvprintw(up_border,   left_border + window_size*i, "%s", labels[i]);
 		mvprintw(down_border, left_border + window_size*i, "%s", empty);
 		mvprintw(down_border, left_border + window_size*i, "%lf", outputs[i]);
	} 

	// Read data from console
	echo();
	for(int i = 0; i < n; i++)
	{
 		mvprintw(down_border, left_border + window_size*i, "%s", empty);
		move(down_border, left_border + window_size*i);
  		curs_set(2);
 		scanw("%lf", &(outputs[i]));
  		curs_set(0);
	} 
 	noecho();
 
	// All works no play label
 	mvprintw(LINES - 2, 0, "Thats OK! Press any key to continue..");
 	getch();
	
	// Back to the Main Menu
 	Screen=0;
}

int NotificationScreen(char *notification_text, char* filename)
{
	int x_size = getmaxx(stdscr);
	// Clear screen
  	attrset(COLOR_PAIR(1));
  	clear();

  	// Draw header
  	attrset(A_BOLD|COLOR_PAIR(2));
  	ClearLine(0, MaxX);
  	mvaddstr(0, 0, notification_text);	
  

  	curs_set(0);
  	refresh();

	if(filename != NULL)
	{
		// Draw body
		int board = 15;
		int up_board = 2;
		int n_vals = 50;
		string line;
  		ifstream myfile ("b.dat");
  		if (myfile.is_open())
 		{	
    			for(int i = 1; getline(myfile, line); i++)
    			{
				if(i % n_vals == 0) 
				{
					if(getch() == KEY_q) break;
					clear();
				
					// Draw header
  					attrset(COLOR_PAIR(2));
  					ClearLine(0, x_size);
		  			mvaddstr(0, 0, notification_text);	

				}
  				attrset(COLOR_PAIR(1));
				int pos = i % n_vals;
   				mvprintw(up_board + pos, board-5, "%4d", i);
   				mvaddstr(up_board + pos, board,   line.c_str());
				
    			}
    			myfile.close();
  		}
  		else mvaddstr(up_board, board,  "Error processing RLSM"); 
	}

	// All works no play label
 	mvprintw(LINES - 2, 0, "Thats OK! Press any key to continue..");
	getch(); 	
	// Back to the Main Menu
 	Screen=0;
	
}

void ClearLine(int y, int l) 
{
	move(y, 1);
	l++;
	char Str[l];
	for (int i = 0; i < l; i++) Str[i] = ' ';
  	Str[l - 1] = '\0';
  	mvaddstr(y, 0, Str);
}

void CatchSIG(int sig) 
{
	Terminated = true;
}

int TUI_Start(lsm_params *lsm_params) 
{  
	signal(SIGINT, CatchSIG);
  
  	initscr();
  	keypad(stdscr, true);
  	nonl(); 
  	cbreak();
  	noecho();   
  	WINDOW *win = newwin(0, 0, 0, 0);
  
	if (has_colors()) 
	{
    		start_color();
    		init_pair(1, COLOR_WHITE,   COLOR_BLACK);
    		init_pair(2, COLOR_GREEN,   COLOR_BLUE);
  	  	init_pair(3, COLOR_BLACK,   COLOR_CYAN);
  	}
  
   	while (!Terminated) 
	{
		Process(lsm_params);
   	 	usleep(1000);
	}
	
  	endwin();
}
