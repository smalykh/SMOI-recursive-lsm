#include <iostream>
#include <errno.h>
#include "armadillo"
#include "lsm_params.h"

using namespace arma;
using namespace std;

int lsm_on_rand_data(double *b_init_vec, double *b_true_vec, lsm_params *params)
{
	int n_inputs = params->n_inputs;
	int n_iters  = params->n_iters;
	
	if((n_inputs <= 0) || (n_iters <= 0)) return -EINVAL;

	colvec b_true(b_true_vec, n_inputs + 1, 1);
	colvec b_init(b_init_vec, n_inputs + 1, 1);
	
	mat U 	= sqrt(params->Du)*randn(n_inputs, n_iters); 
  	U	= join_vert(ones<mat>(1, n_iters) , U);
	mat tmp = trans(U);
	tmp.save("U.dat", raw_ascii);

	mat y = trans(trans(U)*b_true) + sqrt(params->Dr)*randn(1, n_iters);
	y.save("y.dat", raw_ascii);
	
 	mat  b = zeros(n_inputs + 1, n_iters); 
	cube P = zeros(n_inputs + 1, n_inputs + 1, n_iters); 
	mat  K = zeros(n_inputs + 1, n_iters); 

	b.col(0) = b_init;
	P.slice(0) = eye(n_inputs + 1, n_inputs + 1) * params->lambda;

	for(int i = 0; i < n_iters - 1; i++)
	{
		mat p = P.slice(i);
		P.slice(i+1) =  p - 1/as_scalar((1 + trans(U.col(i))*p*U.col(i)))*p*U.col(i)*trans(U.col(i))*p;
		K.col(i+1) = p*U.col(i)/as_scalar((1 + trans(U.col(i))*p*U.col(i)));
		b.col(i+1) = b.col(i) + K.col(i+1)*(y(i) - trans(U.col(i))*b.col(i));
	}

	cout << "b final: " << endl << b.col(n_iters - 1) << endl;

	cout << "Squared errors" << endl;
	mat diffs = b.each_col() - b_true;
	mat sqr_err = trans(sum(diffs%diffs))/n_iters;	
	sqr_err.save("sqr_err.dat", raw_ascii);
	cout << "Final: " << sqr_err(n_iters - 1) << endl;
	
	mat b_readable = join_horiz(trans(b), sqr_err);
	b_readable.save("b.dat", raw_ascii);	
	
	vec errors = zeros(n_iters);
	for(int i = 0; i < n_iters - 10; i++) errors(i) = as_scalar(sum(sqr_err.rows(i, i+10)))/10;
	errors.save("mean_err.dat", raw_ascii);
	return 0;
}
