#include <iostream>
#include <signal.h>
#include <ncurses.h>
#include <menu.h>
#include <string>
#include <unistd.h>
#include <string.h>

using namespace std;

#define SCREEN_MAIN 0

#define KKEY_DOWN 	258
#define KKEY_UP 	259
#define KEY_q 		113
#define KEY_ENTER	'\r'

#define MAIN_MENU_SCREEN	0
#define VAR_SCREEN		1
#define ITER_SCREEN		2
#define B_TRUE_SCREEN		3
#define B_INIT_SCREEN		4
#define LAMBDA_SCREEN		5
#define LSM_SCREEN		6
#define PLOTTER_SCREEN		7

#define MaxX 	getmaxx(stdscr)
#define MaxY 	getmaxy(stdscr)

int ProcessScreenMain();
int InputDataScreen(int n, char **labels, double *outputs, char *header);
int NotificationScreen(char *notification_text, char* filename);
void ClearLine(int y, int l);


