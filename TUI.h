#include "lsm_params.h"
#include <iostream>
#include <fstream>
#include <string>
int  TUI_Start(lsm_params *lsm_params);

// Callback functions
void TUI_Plotter();
int TUI_RLSM(double *b_init_vec, double *b_true_vec, lsm_params *params);

