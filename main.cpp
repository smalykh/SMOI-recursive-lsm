#include <iostream>

#include "armadillo"
#include "lsm_params.h"
#include "TUI.h"

using namespace arma;
using namespace std;

int TUI_RLSM(double *b_init_vec, double *b_true_vec, lsm_params *params)
{
	return lsm_on_rand_data(b_init_vec, b_true_vec, params); 
}

void TUI_Plotter()
{
	system("gnuplot -p -e \"plot 'sqr_err.dat' with l\"  2> /dev/null"); 
	system("gnuplot -p -e \"plot 'mean_err.dat' with l\" 2> /dev/null"); 
}

int main(int argc, char** argv)
{
	cout << "Armadillo version: " << arma_version::as_string() << endl;
  	double b_init_vec[] = {1, 2, 3, 4, 5};
	double b_true_vec[] = {2, 3, 4, 5, 6};
	lsm_params params = {0};
	
	// Start Text User Interface
	TUI_Start(&params);
	
/*	params.n_iters 	= 40;
	params.n_inputs	= 4;
	params.Du	= 50;
	params.Dr	= 0.5;
	params.lambda 	= 1000000;

	lsm_on_rand_data(b_init_vec, b_true_vec, &params); 
	
	system("gnuplot -p -e \"plot 'sqr_err.dat' with l\""); 
	system("gnuplot -p -e \"plot 'mean_err.dat' with l\""); 
*/
  // To see a trace of how Armadillo evaluates expressions,
  // compile with the ARMA_EXTRA_DEBUG macro defined.
  // This was designed to work with the GCC compiler in C++11 mode.
  // 
  // Example for GCC:
  // g++ example2.cpp -o example2 -larmadillo -std=c++11 -DARMA_EXTRA_DEBUG
  // 
  // Running example2 will now produce a truckload of messages,
  // so you may want to redirect the output to a log file.
  
  return 0;
  }

